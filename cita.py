from mastodon import Mastodon
import re
import os
import time
import sys
import os.path
import requests
from PIL import Image, ImageFont, ImageDraw
from bs4 import BeautifulSoup
import textwrap
import pdb

def get_avatar(avatar_url):

    is_saved = False

    try:

        user_agent = {'User-agent': 'Mozilla/5.0'}

        response = requests.get(avatar_url, headers = user_agent, timeout=3)

        if response.status_code == 200:

            avatar_path = 'app/avatar/avatar.'+avatar_url.split('.')[-1]

            with open(avatar_path, 'wb') as f:

                f.write(response.content)

            is_saved = True

        else:

            sys.exit(f'download error: {response.status_code}')

    except requests.exceptions.ConnectionError as conn_error:

        print(f'{conn_error}')

        pass

    return (is_saved, avatar_path)

def create_panel(quote, quoted_user, avatar_path):

    horiz = 10

    vert = 10

    background = Image.open('app/panel/background.png')
    
    quote_image_w, quote_image_h = background.size 
    
    # add avatar_image
    avatar_img = Image.open(avatar_path).convert("RGBA")

    avatar_img = avatar_img.resize((100,100), Image.Resampling.NEAREST)
    
    background.paste(avatar_img, (horiz, vert), avatar_img)

    background.save('app/panel/quote_panel.png',"PNG")

    base = Image.open(f'app/panel/quote_panel.png').convert('RGBA')
    
    txt_image = Image.new('RGBA', base.size, (255,255,255,0))

    txt_image_w, txt_image_h = txt_image.size
    
    fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 15, layout_engine=ImageFont.Layout.BASIC)

    # get a drawing context
    draw = ImageDraw.Draw(txt_image)

    draw.text((horiz,vert+110), quoted_user, font=fnt, fill=(255,255,255,220)) #fill=(255,255,255,255)) ## full opacity

    fnt = ImageFont.truetype('app/fonts/DroidSans.ttf', 30, layout_engine=ImageFont.Layout.BASIC)
    
    lines = textwrap.wrap(quote, width=40)

    y_text = txt_image_h / 2

    for line in lines:

        width, height = fnt.getsize(line)

        draw.text(((txt_image_w - width) / 2, y_text - (height * (len(lines))/2)), line, font=fnt, fill=(255,255,255,220))

        y_text += height
    
    out = Image.alpha_composite(base, txt_image)

    out.save('app/panel/quote_panel.png')

def get_quote_text(content):

    soup = BeautifulSoup(content, features='html.parser')

    delimiter = '###' # unambiguous string

    for line_break in soup.findAll('br'):   # loop through line break tags

        line_break.replaceWith(delimiter)   # replace br tags with delimiter

    quote_str = soup.get_text().split(delimiter)  # get list of strings

    quote = ''
    
    for line in quote_str:

        quote += f'{line}\n'

    return quote

def get_data(notification):

    notification_id = notification.id

    if notification.type != 'mention':

        print(f'dismissing notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

        return

    account_id = notification.account.id

    username = notification.account.acct

    status = notification.status

    visibility = status.visibility

    quote_id = status.in_reply_to_id

    if quote_id == None:

        mastodon.notifications_dismiss(notification_id)

        return

    quote_status_id = status.in_reply_to_id

    quote = get_quote_text(mastodon.status(quote_status_id).content)

    quoted_user = mastodon.status(quote_status_id).account.acct

    avatar = mastodon.status(quote_status_id).account.avatar

    is_saved, avatar_path = get_avatar(avatar)

    if is_saved:

        create_panel(quote, quoted_user, avatar_path)

        panel_image = f'app/panel/quote_panel.png'

        image_id = mastodon.media_post(panel_image, "image/png", description='@cita').id

        toot_text = f'@{quoted_user} @{username}'

        toot_id = mastodon.status_post(toot_text, in_reply_to_id=quote_status_id, visibility=visibility, media_ids={image_id})
        
        print(f'Replied notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

def log_in():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    return mastodon, mastodon_hostname

def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    mastodon, mastodon_hostname = log_in()

    bot_notifications = mastodon.notifications()

    for notif in bot_notifications:

        get_data(notif)
