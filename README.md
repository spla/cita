# cita
Aquest bot està inspirat en el famós @makeitquote, si l'anomenem al respondre una publicació, cita genera una cita de la publicació resposta, incloent-hi l'avatar i el nom d'usuari de la mateixa.  

### Dependencies

-   **Python 3**
-   Un compte de Mastodon per el bot cita

### Com instal·lar cita:

1. Clona el repositori: `git clone https://gitlab.com/spla/cita.git <directori>`  

2. Entra amb `cd` al directori `<directori>` i crea l'Entorn Virtual de Python: `python3.x -m venv .`  
 
3. Activa l'Entorn Virtual de Python: `source bin/activate`  
  
4. Executa `pip install -r requirements.txt` per a afegir les llibreries necessàries.  

5. Executa `python setup.py` per a aconseguir els tokens d'accés del compte del bot en el servidor Mastodon.  

6. Fes servir el teu programador de tasques preferit (per exemple crontab) per a que `python cita.py` s'executi cada minut.  

